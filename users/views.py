from django.views.generic import View
from django.shortcuts import render
from users.forms import ProfileForm
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse

# Create your views here.

def UserProfile(request):
    if request.method == 'POST':
        profile_form = ProfileForm(request.POST)
        if profile_form.is_valid():
           profile = profile_form.save(commit=False)
           profile.user_id = request.user.id
           profile = profile_form.save()
           return HttpResponseRedirect(reverse("src.views.index"))
        else:
            profile_form = ProfileForm(request.POST)
            return render(request,'users/add_profile.html',{'profile_form':profile_form})
    else:
        profile_form = ProfileForm()
        return render(request,'users/add_profile.html',{'profile_form':profile_form})
