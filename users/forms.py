from django import forms
from users.models import Profile

class ProfileForm(forms.ModelForm):
    """Form class for profile."""

    class Meta:
        model = Profile
        exclude = ['user']
    def __init__(self, *args, **kwargs):
        super(ProfileForm, self).__init__(*args, **kwargs)
        self.fields['org_name'].widget.attrs={'class':'form-control'}
        self.fields['email_id'].widget.attrs={'class':'form-control'}
        self.fields['address'].widget.attrs={'class':'form-control'}
        self.fields['contact_number'].widget.attrs={'class':'form-control'}
        self.fields['website'].widget.attrs={'class':'form-control'}

