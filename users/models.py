from django.db import models
from django.contrib.auth.models import User

# Create your models here.

class Profile(models.Model):
    """Model class for adding a profile for registered user."""
    user = models.ForeignKey(User)
    org_name = models.CharField(max_length=100)
    email_id = models.CharField(max_length=50)
    address = models.CharField(max_length=500)
    contact_number = models.IntegerField(max_length=10)
    website = models.CharField(max_length=50)
